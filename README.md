# Service Usage

* FZJ Jupyter

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

* user_metrics_last_1_days.csv - Nr of active accounts in last 1 day
* user_metrics_last_7_days.csv - Nr of active accounts in last 7 day
* user_metrics_last_30_days.csv - Nr of active accounts in last 30 day
* data.csv
* Accounts total - Nr. of total Jupyter-JSC Accounts
* Accounts Helmholtz - Nr. of total Jupyter-JSC Accounts with Helmholtz affiliation
* Accounts new - New accounts of the last day
* Deprovisioned accounts - Deprovisioned accounts of the last day
* Domains total - Number of institutions from which user accounts were measured
* Domains Helmholtz - Number of Helmholtz institutions from which user accounts were measured
* FZJ - Number of accounts coming from FZJ
* AWI - Number of accounts coming from AWI
* ....

## Schedule

* daily

## Weighing

...
